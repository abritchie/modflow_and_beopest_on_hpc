import os
import pandas as pd
import numpy as np
import flopy
import pyemu
import subprocess

#Define working directory
working_dir = os.getcwd()

##Pre-processing section, maybe you have pilot points from pyemu that you want to map to the model grid.

##Run model
subprocess.run(["/home/username/Folder/bin/XXXX.nix", "./input/XXXX.nam"])

##Process output, for example maybe you want to extract output from HYDMOD and save to a file that will be read by PEST using an instruction file.